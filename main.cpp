/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 11 июня 2020 г., 19:42
 *
 * Install: libmysqlcppconn-devs
 *
 */

#include <cstdlib>
#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <iostream>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <thread>
#include "Worker.h"
#include "GlobalData.h"
#include <unistd.h>

using namespace std;

#define MAX_WORKERS 8
#define assrt(x) if(x == -1){ \
                    cerr << strerror(errno) << endl; \
                    }

Worker* worker[MAX_WORKERS];

int main(int , char** ) {
    char cwd[1024];
    if(getcwd(cwd, 1024)){
        cout << cwd << endl;
    }else{
        cerr << strerror(errno) << endl;
    }

    int cpuCnt = thread::hardware_concurrency();
    if(cpuCnt > MAX_WORKERS)
        cpuCnt = MAX_WORKERS;
    cout << "CPU cnt: " << cpuCnt << endl;

    global_data.init();
    
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        cerr << strerror(errno) << endl;
    }
    
    int prm = 1;
    assrt(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &prm, sizeof(int)));
    prm = 1;
    assrt(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &prm, sizeof(int)));

    assrt(fcntl(sock, F_SETFL, O_NONBLOCK));
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(40471);
    assrt(bind(sock, (sockaddr*)&addr, sizeof(sockaddr_in)));
    
    assrt(listen(sock, 0));
    
    for(int i=0; i<cpuCnt; i++){
        worker[i] = new Worker();
        worker[i]->start();
    }
    
    int cur_worker = 0;
    
    while(true){
        fd_set rd;
        FD_SET(sock, &rd);
        timeval tv = {1,0};

        int sel = select(sock+1, &rd, NULL, NULL, &tv);
        assrt(sel);

        if(sel > 0){
            sockaddr_in cli_addr;
            memset(&cli_addr, 0, sizeof(sockaddr_in));
            cli_addr.sin_family = AF_INET;
            socklen_t len = sizeof(sockaddr_in);
            
            int cli_sock = accept(sock, (sockaddr*)&cli_addr, &len);
            assrt(cli_sock);
            
            if(cli_sock > 0){
                cout << "Accept " << inet_ntoa(cli_addr.sin_addr) << endl;
                worker[cur_worker++]->addClient(cli_sock);
                if(cur_worker >= cpuCnt)
                    cur_worker = 0;
            }
        }
    }
    
    
//    sql::Driver* driver = get_driver_instance();
//    if(!driver){
//        cerr << "No driver" << endl;
//    }
//    
//    sql::Connection* con = driver->connect("tcp://127.0.0.1:3306", "www-data", "svtapi71");
//    if(!con){
//        cerr << "No connection" << endl;    
//    }
//    
//    sql::Statement* stmt = con->createStatement();
//    if(!stmt){
//        cerr << "No statement" << endl;    
//    }
//    
//    sql::ResultSet* res = stmt->executeQuery("select * from proj117.param_descr");
//    while(res->next()){
//        cout << res->getString("id") << " = " << res->getString("name") << endl;
//    }
//    delete res;
//    delete stmt;
//    delete con;
    
    return 0;
}

