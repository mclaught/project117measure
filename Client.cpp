/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.cpp
 * Author: user
 * 
 * Created on 11 июня 2020 г., 23:42
 */

#include "Client.h"
#include <string.h>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sys/socket.h>
#include "GlobalData.h"
#include "json.hpp"
#include <sstream>
#include <unistd.h>
#include <netinet/in.h>
#include <fstream>

using namespace nlohmann;

Client::Client() {
}

Client::Client(int sock){
    this->sock = sock;
    //sqlCon = global_data.createConnection();
}

Client::Client(const Client&) {
}

Client::~Client() {
//    global_data.closeConnection(sqlCon);
    if(curFile){
        delete[] curFile;
        cout << "File closed" << endl;
    }
    delete db;
    cout << "Client destroyed" << endl;
}

#define SHIFT(l) p+=l; curSz-=l;pack_data.pack_len-=l
void Client::add(char *buf, int sz){
    logPacket(reinterpret_cast<uint8_t*>(buf), sz);

    memcpy(&buffer[curSz], buf, sz);
    curSz += sz;

    uint8_t* p = reinterpret_cast<uint8_t*>(buffer);

    while(curSz > 0){
        if(!db)
            db = global_data.getDB();

        if(!pack_opened){
            if(curSz >= static_cast<int>(sizeof(hdr_t))){
                hdr_t* hdr = reinterpret_cast<hdr_t*>(p);
                pack_data.pack_n = ntohs(hdr->pack_n);
                pack_data.pack_len = ntohs(hdr->length);
                p += sizeof(hdr_t);
                curSz -= sizeof(hdr_t);
                pack_opened = true;
                pack_data.timestamp = 0;
                values = json::object();

                cout << "========== Start packet " << pack_data.pack_n << " length=" << pack_data.pack_len << " ==========" << endl;

                initAnswer();
            }else{
                break;
            }
        }

        if(curSz < 3)
            break;

        tlv_hdr_t* tlv_hdr = reinterpret_cast<tlv_hdr_t*>(p);
        if(tlv_hdr->tf==0 && tlv_hdr->lf==0){
            tlv1_t* tlv = reinterpret_cast<tlv1_t*>(p);
            if(curSz < (3+tlv->length))
                break;

            pack_data.code = ntohs(tlv->type);
            pack_data.length = tlv->length;
            SHIFT(3);

            cout << "---------- TLV1:" << pack_data.code << ":" << pack_data.length << endl;
        }
        if(tlv_hdr->tf==0 && tlv_hdr->lf==1){
            tlv2_t* tlv = reinterpret_cast<tlv2_t*>(p);
            if(curSz < (4+tlv->length))
                break;

            pack_data.code = ntohs(tlv->type);
            pack_data.length = ntohs(tlv->length) & 0x7FFF;
            SHIFT(4);

            cout << "---------- TLV2:" << pack_data.code << ":" << pack_data.length << endl;
        }
        if(tlv_hdr->tf==1 && tlv_hdr->lf==0){
            ttlv1_t* tlv = reinterpret_cast<ttlv1_t*>(p);
            if(curSz < (7+tlv->length))
                break;

            if(!values.empty())
                db->addData(device.uid, pack_data.pack_n, device.dev_id, pack_data.timestamp, values);
            values = json::object();

            pack_data.code = ntohs(tlv->type) & 0x7FFF;
            pack_data.length = tlv->length;
            pack_data.timestamp = ntohl(tlv->time);
            SHIFT(7);

            cout << "---------- TTLV1:" << pack_data.code << ":" << pack_data.length << ":" << pack_data.timestamp << endl;
        }
        if(tlv_hdr->tf==1 && tlv_hdr->lf==1){
            ttlv2_t* tlv = reinterpret_cast<ttlv2_t*>(p);
            if(curSz < (8+tlv->length))
                break;

            db->addData(device.uid, pack_data.pack_n, device.dev_id, pack_data.timestamp, values);
            values = json::object();

            pack_data.code = ntohs(tlv->type) & 0x7FFF;
            pack_data.length = ntohs(tlv->length) & 0x7FFF;
            pack_data.timestamp = ntohl(tlv->time);
            SHIFT(8);

            cout << "---------- TTLV2:" << pack_data.code << ":" << pack_data.length << ":" << pack_data.timestamp << endl;
        }

        processValues(p);
        processAction(p);
        SHIFT(pack_data.length);

        /*
        if(curSz >= 1 && *p == 0){
            if(pack_data.timestamp && values.size()){
                //Save to DB
                db->addData(device.uid, pack_data.pack_n, device.dev_id, pack_data.timestamp, values);
                values = json::object();
            }
            sendAnswer(&answer);
            pack_opened = false;
            continue;
        }

        if(curSz >= sizeof(uint16_t)){//code
            pack_data.tf = ntohs(*reinterpret_cast<uint16_t*>(p)) & 0x8000 ? 1 : 0;
            pack_data.code = ntohs(*reinterpret_cast<uint16_t*>(p)) & 0x7FFF;
            curSz -= sizeof(uint16_t);
            p += sizeof(uint16_t);
        }else{
            break;
        }
        if(curSz >= 2){//length
            if((*p & 0x80) == 0){
                pack_data.lf = 0;
                pack_data.length = *p & 0x7F;
                p += 1;
                curSz -= 1;
            }else{
                pack_data.lf = 1;
                pack_data.length = ntohs(*reinterpret_cast<uint16_t*>(p)) & 0x7FFF;
                p += 2;
                curSz -= 2;
            }
        }else{
            break;
        }
        if(pack_data.tf){
            if(curSz >= sizeof(tm_t)){
                if(pack_data.timestamp && values.size()){
                    //Save to DB
                    db->addData(device.uid, pack_data.pack_n, device.dev_id, pack_data.timestamp, values);
                }

                values = json::object();
                pack_data.timestamp = ntohl(*reinterpret_cast<tm_t*>(p));
                p += sizeof(tm_t);
                curSz -= sizeof(tm_t);
            }else{
                break;
            }
        }
        if(sz >= pack_data.length){
            processValues(p);
            processAction(p);

            p += pack_data.length;
            curSz -= pack_data.length;
        }else{
            break;
        }
        */
    }

    if(curSz > 0){
        memmove(buffer, p, curSz);
    }

    if(pack_data.pack_len==0){
        if(pack_data.timestamp && values.size()){
            //Save to DB
            db->addData(device.uid, pack_data.pack_n, device.dev_id, pack_data.timestamp, values);
            values = json::object();
        }
        sendAnswer(&answer);
        pack_opened = false;

        cout << "========== End packet ==========" << endl;
    }
}

bool Client::processValues(uint8_t* data){
    if(pack_data.code == 0){
        return false;
    }

    param_descr_t param = global_data.getParamDescr(pack_data.code);
    if(param.id.empty()){
        answer.result = 1;
        cerr << "ERROR: Parameter " << pack_data.code << " not found" << endl;
        return false;
    }

    cout << "Parameter: " << param.name << " = ";

    if(param.type=="int" && pack_data.length==1){
        int8_t value = *reinterpret_cast<int8_t*>(data);
        if(param.log)
            values[param.id] = value;
        cout << value;
    }if(param.type=="int" && pack_data.length==2){
        int16_t value = *reinterpret_cast<int16_t*>(data);
        if(param.log)
            values[param.id] = value;
        cout << value;
    }if(param.type=="int" && pack_data.length==4){
        int32_t value = *reinterpret_cast<int32_t*>(data);
        if(param.log)
            values[param.id] = value;
        cout << value;
    }else if((param.type=="double" || param.type=="float") && pack_data.length==8){
        double value = *reinterpret_cast<double*>(data);
        if(param.log)
            values[param.id] = value;
        cout << value;
    }else if((param.type=="double" || param.type=="float") && pack_data.length==4){
        float value = *reinterpret_cast<float*>(data);
        if(param.log)
            values[param.id] = value;
        cout << value;
    }if(param.type=="str"){
        string value = string(reinterpret_cast<char*>(data), pack_data.length);
        if(param.log)
            values[param.id] = value;
        cout << value;
    }

    cout << endl;

    return true;
}

void Client::processAction(uint8_t* data){
    
    switch(pack_data.code){
        case SP_UID:{
            string uid = string(reinterpret_cast<char*>(data),pack_data.length);
            device = db->getDevice(uid);
            cout << "   Action: UID=" << uid << " dev type=" << device.dev_id << endl;
            break;
        }

        case SP_VER:
            if(device.dev_id){
                cout << "   Action: Version request" << endl;
                addAnswerData(SP_VER, &device.maxver, 2);
            }else{
                setError(2, "Неизвестный UID устройства");
            }
            
            break;
            
        case SP_GETFW:
            if(device.dev_id){
                if(curFile){
                    delete[] curFile;
                    cout << "File closed" << endl;
                }
                curFile = db->getFile(device.dev_id, *reinterpret_cast<uint16_t*>(data), curFileSz);

                if(curFile){
                    cout << "   Action: File request: size=" << curFileSz << endl;
                    addAnswerData(SP_FWSIZE, &curFileSz, 4);
                }else{
                    setError(3, "Файл не найден");
                }
            }else{
                setError(2, "Не известный UID устройства");
            }
            break;
            
        case SP_PART_SHIFT:
            partShift = *reinterpret_cast<uint32_t*>(data);
            if(partShift > curFileSz)
                partShift = curFileSz;
            cout << "   Action: Shift=" << partShift << endl;
            break;
            
        case SP_PART_SIZE:
            if(partShift > curFileSz)
                partShift = curFileSz;
            partSize = *reinterpret_cast<uint16_t*>(data);
            if(partSize > (curFileSz - partShift))
                partSize = curFileSz - partShift;
            cout << "   Action: Part size=" << partSize << endl;
            blob_t blob;
            blob.length = partSize;
            memcpy(blob.data, &curFile[partShift], partSize);
            addAnswerData(SP_PART, &blob, partSize);
            break;
    }
}

void Client::initAnswer(){
    answer.result = 0;
    answer.length = 0;
    ans_data = answer.data;

}

void Client::addAnswerData(uint16_t type, void* value, int sz){
    *reinterpret_cast<uint16_t*>(ans_data) = htons(type);
    ans_data += sizeof(uint16_t);
    answer.length += sizeof(uint16_t);

    if(sz <= 127){
        *ans_data = sz;
        ans_data += 1;
        answer.length += 1;
    }else{
        *reinterpret_cast<uint16_t*>(ans_data) = htons(sz | 0x8000);
        ans_data += 2;
        answer.length += 2;
    }

    memcpy(ans_data, value, sz);
    ans_data += sz;
    answer.length += sz;
}

void Client::sendAnswer(answer_t* answer){
    int len = answer->length;
    answer->length = htons(len);
    if(send(sock, answer, len+3, 0) == -1){
        cerr << "send error: " << strerror(errno) << endl;
    }
}

void Client::closeConnection(){
    close(sock);
}

void Client::setError(uint8_t result, string message){
    cerr << "ERROR: " << result << " " << message << endl;
    answer.result = result;
}

void Client::logPacket(uint8_t* data, int len){
    ofstream file;
    file.open("proj117.log", ios::out | ios::app);
    if(file.good()){
        file << uppercase << setfill('0') << setw(2) << hex;
        for(int i=0; i<len; i++){
            file << uppercase << setfill('0') << setw(2) << hex << (int)data[i] << " ";
            if(((i+1) % 16) == 0)
                file << endl;
        }
    }else{
        cerr << "Log open error: " << strerror(errno) << endl;
    }
    file << endl << endl;
    file.close();
}

//void Client::process(){
//    hdr_t* hdr = reinterpret_cast<hdr_t*>(buffer);
//    char* data = &buffer[sizeof(hdr_t)];
//    
//    //Save common data
//    
//    switch(hdr->cmd){
//        case 0x40:{
//            climate_t* climate = reinterpret_cast<climate_t*>(data);
//            break;
//        }
//        case 0x50:{
//            power_t* power = reinterpret_cast<power_t*>(data);
//            break;
//        }
//        case 0x21:{
//            dev_info_t* dev_info = reinterpret_cast<dev_info_t*>(data);
//            break;
//        }
//        case 0x11:{
//            fw_info_t* fw_info = reinterpret_cast<fw_info_t*>(data);
//            break;
//        }
//        case 0x22:{
//            rssi_t* rssi = reinterpret_cast<rssi_t*>(data);
//            break;
//        }
//        case 0x60:{
//            geo_t* geo = reinterpret_cast<geo_t*>(data);
//            break;
//        }
//    }
//}

