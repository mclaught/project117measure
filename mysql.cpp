/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataMySQL.cpp
 * Author: user
 * 
 * Created on 18 июня 2020 г., 12:39
 */

#include "mysql.h"
#include <mysql_driver.h>
//#include <mysql_connection.h>
#include <mysql_error.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>

DataMySQL::DataMySQL() : DataBase() {
}

DataMySQL::DataMySQL(const DataMySQL& ) : DataBase() {
}

DataMySQL::~DataMySQL() {
    if(con.get()){
        con->close();
        cout << "DB connection closed" << endl;
    }
}

bool DataMySQL::createConnection(){
    if(!con){
        sql::Driver* driver = get_driver_instance();
        if(!driver){
            cerr << "No driver" << endl;
            //throw runtime_error("No driver");
            return false;
        }
        cout << "Driver: " << driver->getName() << endl;


        sql::ConnectOptionsMap connection_properties;

        con = shared_ptr<sql::Connection>(driver->connect("localhost", "proj117", "elcom"));//tcp://localhost:3306
        if(!con.get()){
            cerr << "No connection" << endl;    
            //throw runtime_error("No connection");
            return false;
        }
        cout << "DB connection: MySQL" << endl;
    }
    return con!=nullptr;
}

map<int, param_descr_t> DataMySQL::getParamDescr(){
    map<int, param_descr_t> param_descr;
//    if(!con)
//        return map<int, param_descr_t>();
    
    if(createConnection())
    try{
        
        shared_ptr<sql::PreparedStatement> stmt(con->prepareStatement("select * from proj117.param_descr"));
        shared_ptr<sql::ResultSet> res(stmt->executeQuery());

        while(res->next()){
            param_descr_t pd;
            pd.id = res->getString("id");
            pd.name = res->getString("name");
            pd.type = res->getString("type");
            pd.size = res->getInt("size");
            pd.log = res->getInt("log");

            param_descr[res->getInt("code")] = pd;

            //cout << pd.id << " = " << pd.name << "(" << pd.type << ")" << endl;
        }

//        con->close();
//        delete con;
//        con = nullptr;
    }catch(exception &e){
        cerr << e.what() << endl;
    }

    return param_descr;
}

device_t DataMySQL::getDevice(string uid)
{
    device_t device;

    if(createConnection())
    try{

        shared_ptr<sql::PreparedStatement> stmt(con->prepareStatement(
            "select devices.device_uid, devices.dev_type, max(ver) maxver from proj117.devices \
            left outer join proj117.files on (files.devtype_id = devices.dev_type) \
            where devices.device_uid=? \
            group by devices.device_uid, devices.dev_type"));
        stmt->setString(1, uid);
        shared_ptr<sql::ResultSet> res(stmt->executeQuery());

        if(res->next()){
            device.uid = res->getString(1);
            device.dev_id = res->getInt(2);
            device.maxver = res->getInt(3);

            //cout << pd.id << " = " << pd.name << "(" << pd.type << ")" << endl;
        }
    }catch(exception &e){
        cerr << e.what() << endl;
    }

    return device;
}

void DataMySQL::addData(string uid, int pack_n, uint8_t dev_id, uint32_t timestamp, nlohmann::json values){
    time_t tm = timestamp;
    stringstream tm_stm;
    tm_stm << put_time(localtime(&tm), "%Y-%m-%d %X");

    stringstream val_stm;
    val_stm << values;

    //cout << "Comn data" << endl;
    if(createConnection())
    try{
        shared_ptr<sql::PreparedStatement> stmt(con->prepareStatement("INSERT INTO proj117.data "
            "(`uid`,`pack_n`,`device_type`,`time`,`time_received`,`values`) "
            "VALUES (?,?,?,?,now(),?)"));
        if(stmt){

            sql::SQLString suid(uid);
            stmt->setString(1, suid);
            stmt->setInt(2, pack_n);
            stmt->setInt(3, dev_id);
            stmt->setString(4, tm_stm.str().c_str());
            stmt->setString(5, val_stm.str());
            stmt->executeUpdate();

            stmt->close();
        }
    }catch(exception &e){
        cerr << e.what() << endl;
    }
}

int DataMySQL::getLastFirmwareVer(int devtype_id){
    int ver = 0;
    if(createConnection())
    try{
    shared_ptr<sql::PreparedStatement>stmt(con->prepareStatement("select max(ver) ver from proj117.files where devtype_id=?"));
    if(stmt){
        stmt->setInt(1, devtype_id);
        
        shared_ptr<sql::ResultSet> res(stmt->executeQuery());
        if(res){
            if(res->next()){
                ver = res->getInt(1);
            }
            res->close();
        }
        
        stmt->close();
    }
    }catch(exception &e){
        cerr << e.what() << endl;
    }
    return ver;
}

uint8_t* DataMySQL::getFile(int devtype_id, int ver, uint32_t &sz){
    uint8_t* content = nullptr;

    if(createConnection())
    try{
        istream* stm = nullptr;
        sz = 0;
        shared_ptr<sql::PreparedStatement> stmt(con->prepareStatement("select content, size from proj117.files where devtype_id=? and ver=?"));
        if(stmt){
            stmt->setInt(1, devtype_id);
            stmt->setInt(2, ver);

            shared_ptr<sql::ResultSet> res(stmt->executeQuery());
            if(res){
                if(res->next()){
                    stm = res->getBlob(1);
                    sz = res->getInt(2);
                }
                res->close();
            }

            stmt->close();
        }

        if(stm==0)// || sz==0
            return nullptr;

        stm->seekg(0, std::ios::end);
        sz = stm->tellg();
        stm->seekg(0, std::ios::beg);
//        int pos = stm->tellg();

        content = new uint8_t[sz];
        stm->read(reinterpret_cast<char*>(content), sz);
    }catch(exception &e){
        cerr << e.what() << endl;
    }
    return content;
}
