/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataMySQL.h
 * Author: user
 *
 * Created on 18 июня 2020 г., 12:39
 */

#ifndef DATAMYSQL_H
#define DATAMYSQL_H

#include "basedb.h"
#include <cppconn/connection.h>
#include <memory>

class DataMySQL : public DataBase {
public:
    DataMySQL();
    DataMySQL(const DataMySQL&);
    virtual ~DataMySQL();
    
    virtual bool createConnection();
    virtual map<int, param_descr_t> getParamDescr();
    virtual device_t getDevice(string uid);
    virtual void addData(string uid, int pack_n, uint8_t dev_id, uint32_t timestamp, nlohmann::json values);
    virtual int getLastFirmwareVer(int devtype_id);
    virtual uint8_t* getFile(int devtype_id, int ver, uint32_t &sz);
private:
    shared_ptr<sql::Connection> con;
};

#endif /* DATAMYSQL_H */

