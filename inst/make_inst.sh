#!/bin/bash

BUILDPATH=../build-proj117measure-Desktop_Qt_5_15_0_GCC_64bit-Release
NAME=proj117measure
VER=0.1.0

cp ${BUILDPATH}/proj117measure ${NAME}/usr/sbin
fakeroot dpkg-deb --build ${NAME}
lintian ${NAME}.deb
mv ${NAME}.deb ${NAME}-${VER}-amd64.deb

