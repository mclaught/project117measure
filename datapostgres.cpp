#include "datapostgres.h"
#include <iostream>
#include <pqxx/result>
#include <algorithm>

DataPostgres::DataPostgres()
{

}

DataPostgres::~DataPostgres()
{

}

bool DataPostgres::createConnection(){
    if(!con.get()){
        con = shared_ptr<pqxx::connection>(
                new pqxx::connection("host=localhost port=5432 dbname=proj117 connect_timeout=10 user=proj117 password=elcom")
        );
        cout << "DB connection: PostGRE" << endl;
    }

    return con.get()!=nullptr;
}

std::string ltrim(std::string str, const std::string& chars = "\t\n\v\f\r ")
{
    str.erase(0, str.find_first_not_of(chars));
    return str;
}

std::string rtrim(std::string str, const std::string& chars = "\t\n\v\f\r ")
{
    str.erase(str.find_last_not_of(chars) + 1);
    return str;
}

std::string trim(std::string str, const std::string& chars = "\t\n\v\f\r ")
{
    return ltrim(rtrim(str, chars), chars);
}

map<int, param_descr_t> DataPostgres::getParamDescr(){
    map<int, param_descr_t> param_descr;

    if(createConnection()){
        pqxx::work       xact(*con, "ParamDescr");

        std::string      query("select * from param_descr");
        try {
            pqxx::result res = xact.exec(query);

            for (pqxx::result::const_iterator row = res.begin(), r_end = res.end(); row != r_end; ++row){
                param_descr_t pd;
                pd.id = rtrim(row["id"].c_str());
                pd.name = row["name"].c_str();
                pd.type = rtrim(row["type"].c_str());
                pd.size = row["size"].as(0);
                pd.log = row["log"].as(false);
                param_descr[row["code"].as(0)] = pd;
            }
        } catch (exception &e) {
            cerr << "Postgress error: " << e.what() << endl;
        }
    }

    return param_descr;
}

device_t DataPostgres::getDevice(string uid)
{
    device_t device;

    if(createConnection()){
        stringstream stm;
        stm << "select devices.device_uid, devices.dev_type, max(ver) maxver from devices \
               left outer join files on (files.devtype_id = devices.dev_type) \
               where devices.device_uid='"<<uid<<"' \
               group by devices.device_uid, devices.dev_type";

        cout << stm.str() << endl;

        pqxx::work       xact(*con, "GetDevice");

        std::string      query(stm.str());

        try {
            pqxx::row row = xact.exec1(query);

            device.uid = rtrim(row.at(0).c_str());
            device.dev_id = row.at(1).as(0);
            device.maxver = row.at(2).as(0);
        } catch (exception &e) {
            cerr << "Postgress error: " << e.what() << endl;
        }
    }

    return device;
}

void DataPostgres::addData(string uid, int, uint8_t dev_id, uint32_t timestamp, nlohmann::json values){
    if(createConnection())
    try{
        pqxx::work       xact(*con, "AddData");

        stringstream stm;
        stm << "INSERT INTO data "
            << "(uid,device_type,time,time_received,values) "
            << "VALUES ('"<<uid<<"',"<<(int)dev_id<<",'"<<timestamp<<"',now(),'"<<values<<"')";

        cout << stm.str() << endl;

        xact.exec0(stm.str());
        xact.commit();
    }catch(exception &e){
        cerr << e.what() << endl;
    }
}

int DataPostgres::getLastFirmwareVer(int){
    return 0;
}

uint8_t* DataPostgres::getFile(int devtype_id, int ver, uint32_t &sz){
    uint8_t* content = nullptr;

    if(createConnection()){
        stringstream stm;
        stm << "select content, size from files where devtype_id="<<devtype_id<<" and ver="<<ver;

        pqxx::work       xact(*con, "GetFile");

        std::string      query(stm.str());

        try {
            pqxx::row row = xact.exec1(query);

            pqxx::binarystring bstr(row["content"]);
            sz = bstr.size();
            content = new uint8_t[sz];
            memcpy(content, bstr.get(), sz);
        } catch (exception &e) {
            cerr << "Postgress error: " << e.what() << endl;
        }
    }

    return content;
}
