#ifndef CLIENTUTILS_H
#define CLIENTUTILS_H

/**
 * Библиотека для работы с протоколом сбора информации.
 * Тут используются стандартные функции htons и htonl (перестановка байтов в BigEndian).
 * На всякий случай я их переписал, но можно использовать стандартные, если доступны.
*/

#include <stdint.h>

typedef struct{
    uint16_t pack_n;    //№ пакета
    uint16_t length;
    uint8_t data[];
}packet_t;

/**
 * @brief Начало пакета. Перед заполнение нужно создать буфер, достаточный для размещения всего пакета.
 * @param pack Указатель на буфер
 * @param pack_n № пакета
 */
void start_packet(void* pack, uint16_t pack_n);

/**
 * @brief Добавление TLV-блока произвольного типа
 * @param type Код параметра
 * @param value Указатель на значение
 * @param sz Размер значения
 */
void addTLV(uint16_t type, void* value, int sz);

/**
 * @brief Добавление TTLV-блока произвольного типа
 * @param type Код параметра
 * @param value Указатель на значение
 * @param sz Размер значения
 * @param tm Время
 */
void addTTLV(uint16_t type, void* value, int sz, uint32_t tm);

/**
 * @brief Добавление TLV-блока ординарного типа
 * @param type Код параметра
 * @param value Значение
 */
template <typename T> void addOrdTLV(uint16_t type, T value);

/**
 * @brief Добавление TTLV-блока ординарного типа
 * @param type Код параметра
 * @param value Значение
 * @param tm Время
 */
template <typename T> void addOrdTTLV(uint16_t type, T value, uint32_t tm);

/**
 * @brief Завершение пакета
 * @return Полная длина пакета для передачи
 */
uint16_t end_packet();

#endif // CLIENTUTILS_H
