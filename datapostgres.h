#ifndef DATAPOSTGRES_H
#define DATAPOSTGRES_H

#include "basedb.h"
#include <pqxx/connection>
#include <pqxx/transaction>
#include <memory>

using namespace std;

class DataPostgres : public DataBase
{
public:
    DataPostgres();
    virtual ~DataPostgres();

    virtual bool createConnection();
    virtual map<int, param_descr_t> getParamDescr();
    virtual device_t getDevice(string uid);
    virtual void addData(string uid, int, uint8_t dev_id, uint32_t timestamp, nlohmann::json values);
    virtual int getLastFirmwareVer(int);
    virtual uint8_t* getFile(int devtype_id, int ver, uint32_t &sz);

private:
    shared_ptr<pqxx::connection> con;
};

#endif // DATAPOSTGRES_H
