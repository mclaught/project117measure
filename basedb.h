/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DataBase.h
 * Author: user
 *
 * Created on 18 июня 2020 г., 13:05
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <map>
#include "json.hpp"

using namespace std;

typedef struct{
    string id;
    string name;
    string type;
    int size;
    bool log;
}param_descr_t;

typedef struct{
    string uid;
    int dev_id;
    uint16_t maxver;
}device_t;

class DataBase {
public:
    DataBase();
    DataBase(const DataBase&);
    virtual ~DataBase();
    
    virtual bool createConnection() = 0;
    virtual map<int, param_descr_t> getParamDescr() = 0;
    virtual device_t getDevice(string uid) = 0;
    virtual void addData(string uid, int pack_n, uint8_t dev_id, uint32_t timestamp, nlohmann::json values) = 0;
    virtual int getLastFirmwareVer(int devtype_id) = 0;
    virtual uint8_t* getFile(int devtype_id, int ver, uint32_t &sz) = 0;
private:

};

#endif /* DATABASE_H */

