#!/bin/sh
### BEGIN INIT INFO
# Provides:          signalling
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: RG WebRTC Signallin
# Description:       RG WebRTC Signallin
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="Project117 data service"
NAME=proj117measure
DAEMON=/usr/sbin/${NAME}
PIDFILE=/var/run/${NAME}
LOG=/var/log/proj117.log

case $1 in
    start|restart)
	echo "Starting..."

	cd /
        exec > ${LOG}
        exec 2> ${LOG}
        exec < /dev/null

	${DAEMON} &
	echo $! >> ${PIDFILE}
        ;;

    stop)
	echo "Stopping..."

	pidof ${DAEMON}
	if [ $? -eq 0 ] 
	then
	    PID=$(pidof ${DAEMON})
	    #echo "PID = ${PID}"

	    kill ${PID}
	    #rm ${PIDFILE}
	fi
	;;

    status)
        pidof ${DAEMON}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi
        ;;
esac

	