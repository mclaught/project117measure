/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.h
 * Author: user
 *
 * Created on 11 июня 2020 г., 23:42
 */

#ifndef CLIENT_H
#define CLIENT_H

#include <stdint.h>
#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/exception.h>
#include <cppconn/statement.h>
#include "mysql.h"
#include <memory>
#include "json.hpp"

#define MAX_CLI_BUF 1024*1024

using namespace nlohmann;

enum SPEC_PARAMS{
    SP_UID = 2,
    SP_VER=16,
    SP_GETFW=1000,
    SP_FWSIZE,
    SP_PART_SHIFT,
    SP_PART_SIZE,
    SP_PART
};
enum DATA_MODES{
    MD_SYS,
    MD_LOG
};

#pragma pack(push, 1)

typedef uint32_t tm_t;

typedef struct{
    uint16_t pack_n;    //№ пакета
    uint16_t length;
}hdr_t;

typedef struct{
//    hdr_t hdr;
    uint16_t pack_len;
    uint16_t code;
    uint16_t length;
    uint32_t timestamp;
    uint16_t pack_n;
    char uid[12];
} pack_data_t;

typedef struct {
    uint8_t f0:7;       //Код параметра
    uint8_t tf:1;
    uint8_t f1;
    uint8_t f2:7;    //Размер данных
    uint8_t lf:1;
}tlv_hdr_t;

typedef struct{
    uint16_t type;       //Код параметра
    uint8_t length;    //Размер данных
    uint8_t value[];    //Данные
}tlv1_t;

typedef struct{
    uint16_t type;       //Код параметра
    uint16_t length;    //Размер данных
    uint8_t value[];    //Данные
}tlv2_t;

typedef struct{
    uint16_t type;       //Код параметра
    tm_t time;
    uint8_t length;    //Размер данных
    uint8_t value[];    //Данные
}ttlv1_t;

typedef struct{
    uint16_t type;       //Код параметра
    tm_t time;
    uint16_t length;    //Размер данных
    uint8_t value[];    //Данные
}ttlv2_t;

typedef struct{
    uint16_t length;
    uint8_t data[65535];
}blob_t;
#define TV_SIZE(l) (sizeof(uint16_t)+l)//+sizeof(uint16_t)

//typedef struct{
//    uint16_t type;       //Код параметра
//    uint16_t length;    //Размер данных
//    uint8_t value[];    //Данные
//}tlv_t;
//#define TLV_SIZE(l) (sizeof(uint16_t)*2+l)//+sizeof(uint16_t)

typedef struct{
    uint8_t result;     //Код результата
    uint16_t length;
    uint8_t data[1024*1024];
}answer_t;
#define ANSWER_SIZE(answer) (sizeof(uint8_t)+sizeof(uint8_t)+sizeof(uint16_t)+answer->length)

#pragma pack(pop)

class Client {
public:
    int sock;
    
    Client();
    Client(int sock);
    Client(const Client&);
    virtual ~Client();
    
    void add(char *buf, int sz);
private:
    DataBase* db = nullptr;
    uint8_t buffer[MAX_CLI_BUF];
    int curSz = 0;
    sql::Connection* sqlCon = nullptr;
    uint8_t* curFile = nullptr;
    uint32_t curFileSz = 0;
    uint32_t partShift = 0;
    uint16_t partSize = 0;
    device_t device = {"", 0, 0};
    answer_t answer;
    uint8_t* ans_data;
    bool pack_opened = false;
    pack_data_t pack_data;
    json values;

    bool processValues(uint8_t *data);
    void processAction(uint8_t *data);
    
    //    void process();
    void initAnswer();
    void addAnswerData(uint16_t type, void *value, int sz);
    void sendAnswer(answer_t* answer);
    void closeConnection();
    void setError(uint8_t result, string message);
    void logPacket(uint8_t *data, int len);
};

typedef shared_ptr<Client> PClient;

#endif /* CLIENT_H */

