/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Worker.cpp
 * Author: user
 * 
 * Created on 11 июня 2020 г., 21:54
 */

#include "Worker.h"
#include <sys/epoll.h>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <map>

#define MAX_EVENTS 10
#define assrt(x) if(x == -1){ \
                    cerr << strerror(errno) << endl; \
                    }

Worker::Worker() : MyThread() {
}

Worker::Worker(const Worker&) : MyThread() {
}

Worker::~Worker() {
}

void Worker::addClient(int sock){
    unique_lock<mutex> locker;

    clients[sock] = PClient(new Client(sock));
    
    struct epoll_event ev; 
    ev.data.fd = sock; 
    ev.events = EPOLLIN | EPOLLRDHUP;
    assrt(epoll_ctl(ep, EPOLL_CTL_ADD, sock, &ev));
}

void Worker::closeClient(int sock){
    unique_lock<mutex> locker;

    assrt(epoll_ctl(ep, EPOLL_CTL_DEL, sock, NULL));
    close(sock);
    clients.erase(sock);
}

void Worker::exec(){
    ep = epoll_create(100);
    assrt(ep);
    if(ep == -1)
        return;
    
    while(!terminated){
        struct epoll_event evlist[MAX_EVENTS];
        int ready = epoll_wait(ep, evlist, MAX_EVENTS, 1000);
        if(ready == -1 && errno != EINTR){
            assrt(ready);
        }
        if(ready > 0){
            for(int i=0; i<ready; i++){
                if(evlist[i].events & EPOLLRDHUP){
                    cout << "Closed (epoll)" <<  endl;
                    closeClient(evlist[i].data.fd);
                }else if(evlist[i].events & EPOLLIN)
                    process(evlist[i].data.fd);
            }
        }
    }

    close(ep);
}

bool Worker::process(int sock){
    char buf[1024];
    int rd = read(sock, buf, 1024);
    assrt(rd);
    if(rd > 0){
        unique_lock<mutex> locker;

//        cout << rd << endl;
        map<int, PClient>::iterator fnd = clients.find(sock);
        if(fnd != clients.end())
            fnd->second->add(buf, rd);
    }else if(rd == 0){
        cout << "Closed (read)" <<  endl;
        closeClient(sock);
        return true;
    }
    return false;
}
