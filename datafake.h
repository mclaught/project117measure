#ifndef DATAFAKE_H
#define DATAFAKE_H

#include "basedb.h"
#include <map>
#include "json.hpp"

using namespace std;

class DataFake : public DataBase
{
public:
    DataFake();

    virtual bool createConnection();
    virtual map<int, param_descr_t> getParamDescr();
    virtual device_t getDevice(string uid);
    virtual void addData(string uid, int pack_n, uint8_t dev_id, uint32_t timestamp, nlohmann::json values);
    virtual int getLastFirmwareVer(int);
    virtual uint8_t* getFile(int, int, uint32_t &sz);
private:
    map<int, param_descr_t> param_descr;
    void addParamDescr(int code, string id, string name, string type, int size, bool log);
};

#endif // DATAFAKE_H
