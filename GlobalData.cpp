/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GlobalData.cpp
 * Author: user
 * 
 * Created on 15 июня 2020 г., 13:02
 */

#include "GlobalData.h"
#include <iostream>
#include <cstdlib>
#include <sstream>
#include <stdio.h>
#include "mysql.h"
#include "datafake.h"
#include "datapostgres.h"
#include <memory>
#include "config.h"

GlobalData global_data;

GlobalData::GlobalData() {
}

GlobalData::GlobalData(const GlobalData&) {
}

GlobalData::~GlobalData() {
}

//sql::Connection* GlobalData::createConnection(){
//    sql::Driver* driver = get_driver_instance();
//    if(!driver){
//        cerr << "No driver" << endl;
//        throw runtime_error("No driver");
//    }
//    cout << "Driver: " << driver->getName() << endl;
//    
//    sql::Connection* con = driver->connect("tcp://127.0.0.1:3306", "www-data", "svtapi71");
//    if(!con){
//        cerr << "No connection" << endl;    
//        throw runtime_error("No connection");
//    }
//
//    return con;
//}
//
//void GlobalData::closeConnection(sql::Connection* con){
//    if(con){
//        con->commit();
//        delete con;
//        con = nullptr;
//    }
//}

DataBase* GlobalData::getDB(){
#if(DBTYPE == 1)
    return new DataMySQL();//DataFake();//
#elif (DBTYPE == 0)
    return new DataFake();//
#endif
}

void GlobalData::init(){
    shared_ptr<DataBase> db(getDB());
    
    param_descr = db->getParamDescr();

    for(pair<int, param_descr_t> p : param_descr){
        cout << p.first << "\t" << p.second.id << "\t" << p.second.name << "\t" << p.second.type << "\t" << p.second.size << "\t" << endl;
    }
}

param_descr_t GlobalData::getParamDescr(int code){
    return param_descr[code];
}

bool GlobalData::paramExists(int code){
    return param_descr.find(code)!=param_descr.end();
}

//int GlobalData::addCommonData(sql::Connection* con, string uid, uint16_t train_id, uint8_t car_id, uint16_t recvr_code, uint8_t dev_id, string time, string values){
//    int data_id = -1;
//    
//    cout << "Comn data" << endl;
//    
//    sql::PreparedStatement* stmt = con->prepareStatement("INSERT INTO proj117.data "
//        "(`uid`,`id_train`,`car_number`,`receiver_code`,`device_type`,`time`,`time_received`,`values`) "
//        "VALUES (?,?,?,?,?,?,now(),?)");
//    if(stmt){
//        sql::SQLString suid(uid);
//        stmt->setString(1, suid);
//        stmt->setInt(2, train_id);
//        stmt->setInt(3, car_id);
//        stmt->setInt(4, recvr_code);
//        stmt->setInt(5, dev_id);
//        stmt->setString(6, time);
//        stmt->setString(7, values);
//        stmt->executeUpdate();
//        delete stmt;
//    }
//    
//    stmt->close();
//    delete stmt;
//    
////    sql::Statement* stmt_id = con->createStatement();
////    if(stmt_id){
////        sql::ResultSet* res = stmt_id->executeQuery("select LAST_INSERT_ID() last_id");//execQuery("select LAST_INSERT_ID() last_id");
////        if(res){
////            if(res->next()){
////                data_id = res->getInt("last_id");
////            }
////            //closeQuery(res);
////            delete res;
////        }
////        delete stmt_id;
////    }
//    return data_id;
//}

//void GlobalData::addTypedData(sql::Connection* con, int data_id, string type, string name, char* data, int sz){
//    string table = "data_"+type;
////    stringstream sql_stm;
////    sql_stm << "insert into proj117." << table << "(data_id, param_id, value) values(" << data_id << ", '" << name << "', ";
////    if(type=="int")
////        sql_stm << *reinterpret_cast<int*>(data);
////    else if(type=="double")
////        sql_stm << *reinterpret_cast<double*>(data);
////    else if(type=="str")
////        sql_stm << string(data, sz);
////    sql_stm << ")";
////    string sql = sql_stm.str();
//    
//    //cout << "param_id=" << name << endl;
//    
//    sql::PreparedStatement* stmt = con->prepareStatement("insert into proj117."+table+"(data_id, param_id, value) values"
//            "(?,?,?)");
//    if(stmt){
//        stmt->setInt(1, data_id);
//        stmt->setString(2, name);
//        if(type=="int")
//            stmt->setInt(3, *reinterpret_cast<int*>(data));
//        else if(type=="double")
//            stmt->setDouble(3, *reinterpret_cast<double*>(data));
//        else if(type=="str")
//            stmt->setString(3, string(data, sz));
//        
//        stmt->executeUpdate();
//        
//        delete stmt;
//    }
//    
//    //execUpdate(sql);
//}

//int GlobalData::getLastFirmwareVer(sql::Connection* con, int devtype_id){
//    int ver = 0;
//    sql::PreparedStatement* stmt = con->prepareStatement("select max(ver) ver from proj117.files where devtype_id=?");
//    if(stmt){
//        stmt->setInt(1, devtype_id);
//        
//        sql::ResultSet* res = stmt->executeQuery();
//        if(res){
//            if(res->next()){
//                ver = res->getInt(1);
//            }
//            delete res;
//        }
//        
//        delete stmt;
//    }
//    return ver;
//}

//uint8_t* GlobalData::getFile(sql::Connection* con, int devtype_id, int ver, int &sz){
//    istream* stm = nullptr;
//    sz = 0;
//    sql::PreparedStatement* stmt = con->prepareStatement("select content, size from proj117.files where devtype_id=? and ver=?");
//    if(stmt){
//        stmt->setInt(1, devtype_id);
//        stmt->setInt(2, ver);
//        
//        sql::ResultSet* res = stmt->executeQuery();
//        if(res){
//            if(res->next()){
//                stm = res->getBlob(1);
//                sz = res->getInt(2);
//            }
//            delete res;
//        }
//        
//        delete stmt;
//    }
//    
//    if(stm==0)// || sz==0
//        return nullptr;
//    
//    stm->seekg(0, std::ios::end);
//    sz = stm->tellg();
//    stm->seekg(0, std::ios::beg);
//    int pos = stm->tellg();
//    
//    uint8_t* content = new uint8_t[sz];
//    stm->read(reinterpret_cast<char*>(content), sz);
//    return content;
//}
