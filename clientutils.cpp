#include "clientutils.h"
#include <string.h>

packet_t* packet;
uint8_t* data;
int len = 0;

uint16_t htons(uint16_t x){
    return ((x & 0xFF00) >> 8) | ((x & 0x00FF) << 8);
}

uint32_t htonl(uint32_t x){
    return ((x & 0xFF000000) >> 24) | ((x & 0x00FF0000) >> 8) | ((x & 0x0000FF00) << 8) | ((x & 0x000000FF) << 24);
}

void start_packet(void* pack, uint16_t pack_n){
    packet = (packet_t*)pack;
    packet->pack_n = htons(pack_n);
    data = packet->data;
    len = 0;
}

void addTLV(uint16_t type, void* value, int sz){
    *reinterpret_cast<uint16_t*>(data) = htons(type);
    data += sizeof(uint16_t);
    len += sizeof(uint16_t);

    if(sz <= 127){
        *data = sz;
        data += 1;
        len += 1;
    }else{
        *reinterpret_cast<uint16_t*>(*data) = htons(sz | 0x8000);
        data += 2;
        len += 2;
    }

    memcpy(data, value, sz);
    data += sz;
    len += sz;
}

void addTTLV(uint16_t type, void* value, int sz, uint32_t tm){
    *reinterpret_cast<uint16_t*>(data) = htons((uint16_t)(type | 0x8000));
    data += sizeof(uint16_t);
    len += sizeof(uint16_t);

    *reinterpret_cast<uint32_t*>(data) = htonl(tm);
    data += sizeof(uint32_t);
    len += sizeof(uint32_t);

    if(sz <= 127){
        *data = sz;
        data += 1;
        len += 1;
    }else{
        *reinterpret_cast<uint16_t*>(*data) = htons(sz | 0x8000);
        data += 2;
        len += 2;
    }

    memcpy(data, value, sz);
    data += sz;
    len += sz;
}

template <typename T>
void addOrdTLV(uint16_t type, T value){
    addTLV(type, reinterpret_cast<void*>(&value), sizeof(T));
}

template <typename T>
void addOrdTTLV(uint16_t type, T value, uint32_t tm){
    addTTLV(type, reinterpret_cast<void*>(&value), sizeof(T), tm);
}

uint16_t end_packet(){
    packet->length = htons(len);
    return len + sizeof(uint16_t)*2;
}
