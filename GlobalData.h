/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GlobalData.h
 * Author: user
 *
 * Created on 15 июня 2020 г., 13:02
 */

#ifndef GLOBALDATA_H
#define GLOBALDATA_H

#include <map>
#include "basedb.h"

using namespace std;

class GlobalData {
public:
    GlobalData();
    GlobalData(const GlobalData&);
    virtual ~GlobalData();
    
    DataBase* getDB();
    void init();
    param_descr_t getParamDescr(int code);
    bool paramExists(int code);
    
//    sql::Connection* createConnection();
//    void closeConnection(sql::Connection* con);
//    int addCommonData(sql::Connection* con, string uid, uint16_t train_id, uint8_t car_id, uint16_t recvr_code, uint8_t dev_id, string time, string values);
//    void addTypedData(sql::Connection* con, int data_id, string type, string name, char* data, int sz);
//    int getLastFirmwareVer(sql::Connection* con, int devtype_id);
//    uint8_t* getFile(sql::Connection* con, int devtype_id, int ver, int &sz);
private:
    map<int, param_descr_t> param_descr;
//    sql::Connection* con;
//    sql::Statement* stmt;
//    sql::ResultSet* res;
    
};

extern GlobalData global_data;

#endif /* GLOBALDATA_H */

