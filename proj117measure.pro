TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Client.cpp \
        GlobalData.cpp \
        MyThread.cpp \
        Worker.cpp \
        basedb.cpp \
        clientutils.cpp \
        datafake.cpp \
        datapostgres.cpp \
        main.cpp \
        mysql.cpp

LIBS += -lpthread -lmysqlcppconn -lpqxx

HEADERS += \
    Client.h \
    GlobalData.h \
    MyThread.h \
    Worker.h \
    basedb.h \
    clientutils.h \
    config.h \
    datafake.h \
    datapostgres.h \
    json.hpp \
    mysql.h

INCLUDEPATH += /usr/include/mysql-cppconn-8/jdbc

target.path = /usr/sbin
target.files = proj117measure

#initd.files = proj117.d
initd.path = /etc/init.d
unix:initd.extra = cp proj117.d /etc/init.d; chmod 755 /etc/init.d/proj117.d; update-rc.d proj117.d defaults; service proj117.d start

INSTALLS += target
#INSTALLS += initd
