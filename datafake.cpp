#include "datafake.h"
#include <iostream>

using namespace std;

DataFake::DataFake() : DataBase()
{
}

bool DataFake::createConnection()
{
    cout << "DB connection: FakeDB" << endl;
    return true;
}

void DataFake::addParamDescr(int code, string id, string name, string type, int size,  bool log){
    param_descr_t pd;
    pd.id = id;
    pd.name = name;
    pd.type = type;
    pd.size = size;
    pd.log = log;

    param_descr[code] = pd;
}

map<int, param_descr_t> DataFake::getParamDescr()
{
    if(createConnection()){
        addParamDescr(1, "time", "Время", "time", 4, false);
        addParamDescr(2, "uid", "UID", "str", 12, false);
        addParamDescr(3, "pack_n", "3 пакета", "int", 4, false);
        addParamDescr(14, "rssif", "RSSI прямой", "int", 1, true);
        addParamDescr(15, "rssib", "RSSI обратный", "int", 1, true);
        addParamDescr(16, "sw", "Версия", "int", 2, false);
        addParamDescr(17, "phn", "Телефон", "int", 4, false);
        addParamDescr(18, "blnc", "Баланс", "int", 2, true);
        addParamDescr(19, "ontm", "On time", "int", 4, true);
        addParamDescr(20, "offtm", "Off time", "int", 4, true);
        addParamDescr(101, "lat", "Широта", "double", 8, true);
        addParamDescr(102, "lon", "Долгота", "double", 8, true);
        addParamDescr(103, "alt", "высота", "double", 8, true);
        addParamDescr(104, "spd", "Скорость", "double", 8, true);
        addParamDescr(105, "brn", "Азимут", "double", 8, true);
        addParamDescr(201, "I", "Ток", "int", 2, true);
        addParamDescr(202, "V", "Напряжение", "int", 2, true);
        addParamDescr(203, "Pa", "мощность акт.", "int", 4, true);
        addParamDescr(204, "Pr", "мощность реакт.", "int", 4, true);
        addParamDescr(301, "chnl1", "Канал 1", "int", 1, true);
        addParamDescr(302, "chnl2", "Канал 2", "int", 1, true);
        addParamDescr(303, "chnl3", "Канал 3", "int", 1, true);
        addParamDescr(304, "chnl4", "Канал 4", "int", 1, true);
        addParamDescr(1000, "getfw", "Запрос прошивки", "int", 2, false);
        addParamDescr(1001, "filsz", "Размер файла", "int", 4, false);
        addParamDescr(1002, "shft", "Смещение", "int", 4, false);
        addParamDescr(1003, "getfl", "Размер блока", "int", 2, false);
        addParamDescr(1004, "flидл", "Блок", "blob", 0, false);
    }

    return param_descr;
}

device_t DataFake::getDevice(string uid)
{
    device_t dev;
    dev.uid = uid;
    dev.dev_id = 18;
    dev.maxver = 4;
    return dev;
}

void DataFake::addData(string uid, int pack_n, uint8_t dev_id, uint32_t timestamp, nlohmann::json values)
{
    cout << "Add to DB: UID=" << uid << " pack_n=" << pack_n << " dev=" << dev_id << " time=" << timestamp << " " << values << endl;
}

int DataFake::getLastFirmwareVer(int)
{
    return 4;
}

uint8_t *DataFake::getFile(int, int, uint32_t &sz)
{
    sz = 10000;

    uint8_t* file = new uint8_t[sz];

    for(int i=0; i<10000; i++)
        file[i] = rand() & 0xFF;

    return file;
}
