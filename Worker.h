/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Worker.h
 * Author: user
 *
 * Created on 11 июня 2020 г., 21:54
 */

#ifndef WORKER_H
#define WORKER_H

#include <map>
#include "MyThread.h"
#include "Client.h"
#include <memory>
#include <mutex>

using namespace std;

class Worker : public MyThread {
public:
    Worker();
    Worker(const Worker&);
    virtual ~Worker();
    
    void addClient(int sock);
    void closeClient(int sock);
private:
    map<int, PClient> clients;
    int ep;
    mutex mtx;
    
    virtual void exec();
    bool process(int sock);
};

#endif /* WORKER_H */

