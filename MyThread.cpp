/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.cpp
 * Author: user
 * 
 * Created on 16 января 2017 г., 9:56
 */

#include <sys/syslog.h>
#include <system_error>
#include <iostream>
#include "MyThread.h"

MyThread::MyThread() : terminated(false)
{
//    _thrd = NULL;
}

MyThread::MyThread(const MyThread&)
{
}

MyThread::~MyThread()
{
    terminate();
//    if(_thrd)
//    {
//        terminate();
//        _thrd->detach();
//        delete _thrd;
//    }
}

void MyThread::start()
{
    terminated = false;
    _thrd = thread(_thread_func, this);
}

void MyThread::terminate()
{
    try{
        terminated = true;
        if(_thrd.joinable())
            _thrd.join();
    }catch(system_error &e){
        cerr << e.what() << endl;
    }
}

void MyThread::_thread_func(MyThread* _this)
{
    _this->exec();
}
